import React, { Component } from 'react';
import {
  DrawerNavigator,
  StackNavigator,
  TabNavigator
} from 'react-navigation';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  StatusBar
} from 'react-native';
import {
  Card,
  ListItem,
  Button
} from 'react-native-elements';
import addFeeds from '../feedReader.js';

let htmlparser = require("htmlparser2-without-node-native");

let desc = '';
let img_src = '';
let parser = new htmlparser.Parser({
  onopentag: function(name, attr) {
    if(name == 'img') {
      img_src = attr.src;
    }
  },
  ontext: function(text){
    desc = text;
    return text;
  },
}, {decodeEntities: true});

class NewsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }

  componentDidMount() {
    addFeeds()
      .then(res => {
        for(i in res) {
          parser.write(res[i].description);
          res[i].description = desc;
          res[i].image = img_src;
          res[i].key = i;
        }
        const posts = res;
        this.setState({posts});
      });
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        {this.state.posts.map(post => 
          <Card key={post.key} title={post.title} image={{uri: post.image}}>
            <Text style={{marginBottom: 10}}>
              {post.description}
            </Text>
            <Button
              icon={{name: 'code'}}
              backgroundColor='#03A9F4'
              fontFamily='Lato'
              buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
              title='VIEW NOW' />
          </Card>
        )}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20
  }
});

class ProfilesScreen extends React.Component {
  render() {
    return <Text>List of all contacts</Text>
  }
}

const MainScreenNavigator = TabNavigator({
  News: { screen: NewsScreen },
  Profiles: { screen: ProfilesScreen },
});

class AdmissionsScreen extends React.Component {
  render() {
    return <Text>List of lalala</Text>
  }
}

class UniversitiesScreen extends React.Component {
  render() {
    return <Text>List of lalala</Text>
  }
}

const SimpleApp = DrawerNavigator({
  Home: { 
    screen: MainScreenNavigator,
    navigationOptions: {
      title: 'Home',
    },
  },
  Admissions: { screen: AdmissionsScreen },
  Universities: { screen: UniversitiesScreen },
})

// export default SimpleApp;

export default class App extends Component{
  componentDidMount() {
    StatusBar.setHidden(true);
  }
  render() {
    return (
      <SimpleApp />
    )
  }
}