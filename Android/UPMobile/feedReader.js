import axios from 'axios';
import React, {Component} from 'react';
import { Card, ListItem, Button } from 'react-native-elements';
import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
var parseString = require('react-native-xml2js').parseString;

const rssUrlNews = 'https://web01.up.edu.ph/index.php/category/news/feed/';
const rssUrlAnnouncement = 'https://web01.up.edu.ph/index.php/category/announcements/feed/';

const addFeeds = function (){
	return axios.get("https://api.rss2json.com/v1/api.json?rss_url=" +rssUrlNews)
    .then(res => {
      //console.log(res.data.items);
      return res.data.items;
    })
    .catch(err => {
    	console.log(err);
    });
}

export default addFeeds;