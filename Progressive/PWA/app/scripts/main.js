/*!
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */
/* eslint-env browser */
(function() {
  'use strict';

  // Check to make sure service workers are supported in the current browser,
  // and that the current page is accessed from a secure origin. Using a
  // service worker from an insecure origin will trigger JS console errors. See
  // http://www.chromium.org/Home/chromium-security/prefer-secure-origins-for-powerful-new-features
  var isLocalhost = Boolean(window.location.hostname === 'localhost' ||
      // [::1] is the IPv6 localhost address.
      window.location.hostname === '[::1]' ||
      // 127.0.0.1/8 is considered localhost for IPv4.
      window.location.hostname.match(
        /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
      )
    );

  if ('serviceWorker' in navigator &&
      (window.location.protocol === 'https:' || isLocalhost)) {
    navigator.serviceWorker.register('service-worker.js')
    .then(function(registration) {
      // updatefound is fired if service-worker.js changes.
      registration.onupdatefound = function() {
        // updatefound is also fired the very first time the SW is installed,
        // and there's no need to prompt for a reload at that point.
        // So check here to see if the page is already controlled,
        // i.e. whether there's an existing service worker.
        if (navigator.serviceWorker.controller) {
          // The updatefound event implies that registration.installing is set:
          // https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#service-worker-container-updatefound-event
          var installingWorker = registration.installing;

          installingWorker.onstatechange = function() {
            switch (installingWorker.state) {
              case 'installed':
                // At this point, the old content will have been purged and the
                // fresh content will have been added to the cache.
                // It's the perfect time to display a "New content is
                // available; please refresh." message in the page's interface.
                break;

              case 'redundant':
                throw new Error('The installing ' +
                                'service worker became redundant.');

              default:
                // Ignore
            }
          };
        }
      };
    }).catch(function(e) {
      console.error('Error during service worker registration:', e);
    });
  }

  // Your custom JavaScript goes here
})();
const rssUrlNews = 'https://web01.up.edu.ph/index.php/category/news/feed/';
const rssUrlAnnouncement = 'https://web01.up.edu.ph/index.php/category/announcements/feed/';
const rssUrlBreakthroughs = 'https://web01.up.edu.ph/index.php/category/breakthroughs/feed/';
const rssUrlProfile = 'https://web01.up.edu.ph/index.php/category/profiles/feed/';
/* eslint-disable */
const addFeeds = function(containerName, mod) {
/* eslint-enable */
  var rssUrl;
  if (mod === 'N') {
    rssUrl = rssUrlNews;
  } else if (mod === 'A') {
    rssUrl = rssUrlAnnouncement;
  } else if (mod === 'B') {
    rssUrl = rssUrlBreakthroughs;
  } else if (mod === 'P') {
    rssUrl = rssUrlProfile;
  }
  const container = $('#' + containerName);
  $.ajax({
    type: 'GET',
    url: 'https://api.rss2json.com/v1/api.json?rss_url=' + rssUrl,
    dataType: 'jsonp',

    success: function(data) {
      // console.log(data);
      data.items.forEach(function(idx) {
        addItem(container, idx.title, idx.description, idx.pubDate, idx.content);
      });
      // $('#loadingIndicator').hide();
      // $.getScript('scripts/showhide.js');
    },
    error: function() {
      console.log('fail');
    }
  });
};

function addItem(container, itemTitle, itemDescription, itemPubdate, itemContent) {
  var desc = itemDescription.replace(/<img .*>/, '');
  var cont = itemContent.match(/src=\"(.*?)\"/);
  var cont2 = itemContent.replace(/<img .*\/>/, '');

  container.append(`
    <div class="card">
      <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src=${cont[1]}>
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">${itemTitle}</span>
        <p class="activator">${desc}</p>

      </div>
      <div class="card-reveal">
        <span class="card-title activator grey-text text-darken-4">${itemTitle}<i class="material-icons right">close</i></span>
        <p>${cont2}</p>
      </div>
    </div>
    `);
}
