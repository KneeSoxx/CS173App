// $('#toggle').click(function() {
//   $('#contentHolder > .toggleMe').next().toggle();
//   $(this).text(function(i, txt) {
//     return txt === 'Show More' ? 'Show Less' : 'Show More';
//   });
// });

// $('.toggle').click(function() {
// 	$(this).closest('.contentHolder').find('.toggleMe').toggleClass('visible');
// })
/* eslint-disable */
$(document).on('click', '#toggle', function() {
/* eslint-enable */
  var parent = $(this).closest('div.mdl-card-wide');
  parent.find('div.toggleMe').toggle();
  $(this).text(function(i, txt) {
    return txt === 'Show More' ? 'Show Less' : 'Show More';
  });
});
